+++
showonlyimage = false
draft = false
date = "2016-11-05T18:25:22+05:30"
title = "Tern Link C7"
weight = 0
tags = ["ciclismo", "deporte"]
price = 35000
+++

Bicicleta plegable con 7 cambios.

<!--more-->
Detalles:

- Necesita un cambio de cubierta

{{<photos>}}
